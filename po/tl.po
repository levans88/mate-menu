# Tagalog translation for MATE Menu
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: mate-menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-14 00:28+0100\n"
"PO-Revision-Date: 2011-08-24 16:03+0000\n"
"Last-Translator: Libni Pacheco <Unknown>\n"
"Language-Team: Tagalog <tl@li.org>\n"
"Language: tl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-25 10:21+0000\n"
"X-Generator: Launchpad (build 17355)\n"

#: ../lib/mate-menu.py:69
msgid "Menu"
msgstr "Menu"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:236
msgid "Couldn't load plugin:"
msgstr "Hindi mai-load ang plugin:"

#: ../lib/mate-menu.py:309
msgid "Couldn't initialize plugin"
msgstr "Hindi ma-initialize ang plugin"

#: ../lib/mate-menu.py:740
msgid "Advanced MATE Menu"
msgstr ""

#: ../lib/mate-menu.py:834
msgid "Preferences"
msgstr "Mga Kagustuhan"

#: ../lib/mate-menu.py:837
msgid "Edit menu"
msgstr "Baguhin ang menu"

#: ../lib/mate-menu.py:840
msgid "Reload plugins"
msgstr "I-karga muli ang mga plugin"

#: ../lib/mate-menu.py:843
msgid "About"
msgstr "Tungkol dito"

#. i18n
#: ../lib/mate-menu-config.py:53
msgid "Menu preferences"
msgstr "Kagustuhan para sa menu"

#: ../lib/mate-menu-config.py:56
msgid "Always start with favorites pane"
msgstr "Laging magsimula gamit ang favorites pane"

#: ../lib/mate-menu-config.py:57
msgid "Show button icon"
msgstr "Ipakita ang icon ng pindutan"

#: ../lib/mate-menu-config.py:58
msgid "Use custom colors"
msgstr "Gumamit ng nais na kulay"

#: ../lib/mate-menu-config.py:59
msgid "Show recent documents plugin"
msgstr "Ipakita ang plugin para sa mga document na ginamit kamakailan"

#: ../lib/mate-menu-config.py:60
msgid "Show applications plugin"
msgstr "Ipakita ang plugin para sa mga application"

#: ../lib/mate-menu-config.py:61
msgid "Show system plugin"
msgstr "Ipakita ang plugin para sa system"

#: ../lib/mate-menu-config.py:62
msgid "Show places plugin"
msgstr "Ipakita ang plugin para sa \"places\""

#: ../lib/mate-menu-config.py:64
msgid "Show application comments"
msgstr "Ipakita ang mga komento sa application"

#: ../lib/mate-menu-config.py:65
msgid "Show category icons"
msgstr "Ipakita ang mga icon ng kategorya"

#: ../lib/mate-menu-config.py:66
msgid "Hover"
msgstr "Itapat"

#: ../lib/mate-menu-config.py:67
msgid "Remember the last category or search"
msgstr ""

#: ../lib/mate-menu-config.py:68
msgid "Swap name and generic name"
msgstr "Pagpalitin ang pangalan at generic na pangalan"

#: ../lib/mate-menu-config.py:70
msgid "Border width:"
msgstr "Kapal ng border:"

#: ../lib/mate-menu-config.py:71
msgid "pixels"
msgstr "mga pixel"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "Sulat ng pindutan"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "Mga Pagpipilian"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:189
msgid "Applications"
msgstr "Mga Application"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "Tema"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:186
#: ../mate_menu/plugins/applications.py:187
msgid "Favorites"
msgstr "Mga Paborito"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "Pangunahing pindutan (Main button)"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr ""

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "Background:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr "Mga heading:"

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "Mga border:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "Tema:"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "Bilang ng mga kolum:"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "Laki ng icon:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr "Delay ng pagtapat (ms):"

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "Icon ng pindutan:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "Command para sa paghahanap:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:54
msgid "Places"
msgstr "Mga Lugar (Places)"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr "Payagan ang Scrollbar"

#: ../lib/mate-menu-config.py:100
msgid "Show GTK+ Bookmarks"
msgstr "Ipakita ang mga GTK+ Bookmark"

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "Taas:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr "I-toggle ang mga Default na mga Lugar:"

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:149
msgid "Computer"
msgstr "Computer"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:160
msgid "Home Folder"
msgstr "Folder para sa Home"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:173
msgid "Network"
msgstr "Network"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:196
msgid "Desktop"
msgstr "Desktop"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:207
msgid "Trash"
msgstr "Basura"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "Mga Custom na Lugar:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "System"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr "I-toggle ang mga Default na Item:"

#: ../lib/mate-menu-config.py:114 ../mate_menu/plugins/system_management.py:153
#: ../mate_menu/plugins/system_management.py:156
#: ../mate_menu/plugins/system_management.py:159
#: ../mate_menu/plugins/system_management.py:162
msgid "Package Manager"
msgstr "Tagapamahala ng Package"

#: ../lib/mate-menu-config.py:115 ../mate_menu/plugins/system_management.py:172
msgid "Control Center"
msgstr "Control Center"

#: ../lib/mate-menu-config.py:116 ../mate_menu/plugins/system_management.py:179
msgid "Terminal"
msgstr "Terminal"

#: ../lib/mate-menu-config.py:117 ../mate_menu/plugins/system_management.py:193
msgid "Lock Screen"
msgstr "I-lock ang Screen"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "Mag Log Out"

#: ../lib/mate-menu-config.py:119 ../mate_menu/plugins/system_management.py:211
msgid "Quit"
msgstr "Tapusin at Umalis"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "Baguhin ang Lugar"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "Bagong Lugar"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "Pumili ng Folder"

#: ../lib/mate-menu-config.py:151
msgid "Keyboard shortcut:"
msgstr "Shortcut sa Keyboard:"

#: ../lib/mate-menu-config.py:157
msgid "Images"
msgstr ""

#: ../lib/mate-menu-config.py:265
msgid "Name"
msgstr "Pangalan"

#: ../lib/mate-menu-config.py:266
msgid "Path"
msgstr "Landas (Path)"

#: ../lib/mate-menu-config.py:282
msgid "Desktop theme"
msgstr "Tema ng Desktop"

#: ../lib/mate-menu-config.py:423 ../lib/mate-menu-config.py:454
msgid "Name:"
msgstr "Pangalan:"

#: ../lib/mate-menu-config.py:424 ../lib/mate-menu-config.py:455
msgid "Path:"
msgstr "Landas (Path):"

#. i18n
#: ../mate_menu/plugins/applications.py:184
msgid "Search:"
msgstr "Maghanap:"

#: ../mate_menu/plugins/applications.py:188
msgid "All applications"
msgstr "Lahat ng mga Application"

#: ../mate_menu/plugins/applications.py:552
#, python-format
msgid "Search Google for %s"
msgstr "Hanapin sa Google ang %s"

#: ../mate_menu/plugins/applications.py:559
#, python-format
msgid "Search Wikipedia for %s"
msgstr "Hanapin sa Wikipedia ang %s"

#: ../mate_menu/plugins/applications.py:576
#, python-format
msgid "Lookup %s in Dictionary"
msgstr "Hanapin ang %s sa Diksyunaryo"

#: ../mate_menu/plugins/applications.py:583
#, python-format
msgid "Search Computer for %s"
msgstr "Hanapin sa Computer ang %s"

#. i18n
#: ../mate_menu/plugins/applications.py:703
#: ../mate_menu/plugins/applications.py:768
msgid "Add to desktop"
msgstr "Idagdag sa Desktop"

#: ../mate_menu/plugins/applications.py:704
#: ../mate_menu/plugins/applications.py:769
msgid "Add to panel"
msgstr "Idagdag sa Panel"

#: ../mate_menu/plugins/applications.py:706
#: ../mate_menu/plugins/applications.py:747
msgid "Insert space"
msgstr "Maglagay ng space"

#: ../mate_menu/plugins/applications.py:707
#: ../mate_menu/plugins/applications.py:748
msgid "Insert separator"
msgstr "Maglagay ng separator"

#: ../mate_menu/plugins/applications.py:709
#: ../mate_menu/plugins/applications.py:772
msgid "Launch"
msgstr "Simulan"

#: ../mate_menu/plugins/applications.py:710
msgid "Remove from favorites"
msgstr "Tanggalin sa mga paborito"

#: ../mate_menu/plugins/applications.py:712
#: ../mate_menu/plugins/applications.py:775
msgid "Edit properties"
msgstr "Baguhin ang mga property"

#. i18n
#: ../mate_menu/plugins/applications.py:746
msgid "Remove"
msgstr "Alisin"

#: ../mate_menu/plugins/applications.py:771
msgid "Show in my favorites"
msgstr "Ipakita sa mga paborito"

#: ../mate_menu/plugins/applications.py:773
msgid "Delete from menu"
msgstr "Tanggalin sa menu"

#: ../mate_menu/plugins/applications.py:817
msgid "Search Google"
msgstr "Maghanap sa Google"

#: ../mate_menu/plugins/applications.py:824
msgid "Search Wikipedia"
msgstr "Maghanap sa Wikipedia"

#: ../mate_menu/plugins/applications.py:834
msgid "Lookup Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:841
msgid "Search Computer"
msgstr "Maghanap sa Computer"

#: ../mate_menu/plugins/applications.py:1245
#, fuzzy
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""
"Hindi mai-save ang mga paborito. Siguraduhing pwede kang magsulat sa ~/."
"linuxmint/mintMenu"

#: ../mate_menu/plugins/applications.py:1452
msgid "All"
msgstr "Lahat"

#: ../mate_menu/plugins/applications.py:1452
msgid "Show all applications"
msgstr "Ipakita lahat ang mga application"

#: ../mate_menu/plugins/system_management.py:167
msgid "Install, remove and upgrade software packages"
msgstr "Mag-install, magtanggal at mag-upgrade ng mga software package"

#: ../mate_menu/plugins/system_management.py:176
msgid "Configure your system"
msgstr "Ayusin ang iyong system"

#: ../mate_menu/plugins/system_management.py:190
msgid "Use the command line"
msgstr "Gamitin ang command line"

#: ../mate_menu/plugins/system_management.py:201
msgid "Requires password to unlock"
msgstr "Kailangan ng password para mabuksan"

#: ../mate_menu/plugins/system_management.py:204
msgid "Logout"
msgstr "Mag-logout"

#: ../mate_menu/plugins/system_management.py:208
msgid "Log out or switch user"
msgstr "Mag-logout o magpalit ng user"

#: ../mate_menu/plugins/system_management.py:215
msgid "Shutdown, restart, suspend or hibernate"
msgstr "I-shutdown, i-restart, i-suspend o mag-hibernate"

#: ../mate_menu/plugins/places.py:157
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr ""
"Tingnan ang lahat ng local at remote na mga disk at mga folder na kayang i-"
"access ng computer na ito"

#: ../mate_menu/plugins/places.py:168
msgid "Open your personal folder"
msgstr "Buksan ang iyong personal folder"

#: ../mate_menu/plugins/places.py:181
msgid "Browse bookmarked and local network locations"
msgstr "Tingnan ang mga nai-bookmark na local network locations"

#: ../mate_menu/plugins/places.py:204
msgid "Browse items placed on the desktop"
msgstr "Tingnan ang mga item na inilagay sa desktop"

#: ../mate_menu/plugins/places.py:217
msgid "Browse deleted files"
msgstr "Tingnan ang mga binurang file"

#: ../mate_menu/plugins/places.py:270
msgid "Empty trash"
msgstr "Linisin ang basura"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "Mga document kamakailan"

#: ../mate_menu/keybinding.py:170
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""

#: ../mate_menu/keybinding.py:171
msgid "Press Escape or click again to cancel the operation.  "
msgstr ""

#: ../mate_menu/keybinding.py:172
msgid "Press Backspace to clear the existing keybinding."
msgstr ""

#: ../mate_menu/keybinding.py:191
msgid "Pick an accelerator"
msgstr ""

#: ../mate_menu/keybinding.py:247
msgid "<not set>"
msgstr ""

#~ msgid "Opacity:"
#~ msgstr "Opacity:"

#~ msgid "Launch when I log in"
#~ msgstr "Simulan kapag nag-log in ako"

#~ msgid "Software Manager"
#~ msgstr "Tagapamahala ng Software"

#~ msgid "Install package '%s'"
#~ msgstr "I-install ang package na '%s'"

#~ msgid "Uninstall"
#~ msgstr "I-uninstall"

#~ msgid "Find Software"
#~ msgstr "Maghanap ng Software"

#~ msgid "Find Tutorials"
#~ msgstr "Maghanap ng mga Tutorial"

#~ msgid "Find Hardware"
#~ msgstr "Maghanap ng Hardware"

#~ msgid "Find Ideas"
#~ msgstr "Maghanap ng mga ideya"

#~ msgid "Find Users"
#~ msgstr "Mgahanap ng mga User"

#~ msgid "Browse and install available software"
#~ msgstr "Maghanap at mag-install ng mga available na software"

#~ msgid "Based on USP from S.Chanderbally"
#~ msgstr "Hango sa USP ni S.Chanderbally"

#~ msgid "Please wait, this can take some time"
#~ msgstr "Mangyaring maghintay, ito ay maaaring tumagal ng ilang minuto"

#~ msgid "Advanced Gnome Menu"
#~ msgstr "Advance na Menu para sa Gnome"

#~ msgid "Application removed successfully"
#~ msgstr "Natanggal ng maayos ang application"

#~ msgid "Do you want to remove this menu entry?"
#~ msgstr "Gusto mo bang tanggalin ang menu entry na ito?"

#~ msgid "Packages to be removed"
#~ msgstr "Mga package na tatanggalin"

#~ msgid "Remove %s?"
#~ msgstr "Tanggalin ang %s?"

#~ msgid "The following packages will be removed:"
#~ msgstr "Ang mga sumusunod na package ay tatanggalin:"

#~ msgid "No matching package found"
#~ msgstr "Walang package na magmamatch ang nakita"
