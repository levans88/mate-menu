# This file is distributed under the same license as the mate-menu tool.
# Copyright by the copyright holders of the mate-menu tool.
msgid ""
msgstr ""
"Project-Id-Version: mate-menu VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-14 00:28+0100\n"
"PO-Revision-Date: 2014-06-29 14:28+0100\n"
"Last-Translator: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>\n"
"Language-Team: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: English\n"
"X-Poedit-Country: UNITED STATES\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../lib/mate-menu.py:69
msgid "Menu"
msgstr "Menu"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:236
msgid "Couldn't load plugin:"
msgstr "Couldn't load plugin:"

#: ../lib/mate-menu.py:309
msgid "Couldn't initialize plugin"
msgstr "Couldn't initialize plugin"

#: ../lib/mate-menu.py:740
msgid "Advanced MATE Menu"
msgstr "Advanced MATE Menu"

#: ../lib/mate-menu.py:834
msgid "Preferences"
msgstr "Preferences"

#: ../lib/mate-menu.py:837
msgid "Edit menu"
msgstr "Edit menu"

#: ../lib/mate-menu.py:840
msgid "Reload plugins"
msgstr "Reload plugins"

#: ../lib/mate-menu.py:843
msgid "About"
msgstr "About"

#. i18n
#: ../lib/mate-menu-config.py:53
msgid "Menu preferences"
msgstr "Menu preferences"

#: ../lib/mate-menu-config.py:56
msgid "Always start with favorites pane"
msgstr "Always start with favorites pane"

#: ../lib/mate-menu-config.py:57
msgid "Show button icon"
msgstr "Show button icon"

#: ../lib/mate-menu-config.py:58
msgid "Use custom colors"
msgstr "Use custom colors"

#: ../lib/mate-menu-config.py:59
msgid "Show recent documents plugin"
msgstr "Show recent documents plugin"

#: ../lib/mate-menu-config.py:60
msgid "Show applications plugin"
msgstr "Show applications plugin"

#: ../lib/mate-menu-config.py:61
msgid "Show system plugin"
msgstr "Show system plugin"

#: ../lib/mate-menu-config.py:62
msgid "Show places plugin"
msgstr "Show places plugin"

#: ../lib/mate-menu-config.py:64
msgid "Show application comments"
msgstr "Show application comments"

#: ../lib/mate-menu-config.py:65
msgid "Show category icons"
msgstr "Show category icons"

#: ../lib/mate-menu-config.py:66
msgid "Hover"
msgstr "Hover"

#: ../lib/mate-menu-config.py:67
msgid "Remember the last category or search"
msgstr "Remember the last category or search"

#: ../lib/mate-menu-config.py:68
msgid "Swap name and generic name"
msgstr "Swap name and generic name"

#: ../lib/mate-menu-config.py:70
msgid "Border width:"
msgstr "Border width:"

#: ../lib/mate-menu-config.py:71
msgid "pixels"
msgstr "pixels"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "Button text:"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "Options"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:189
msgid "Applications"
msgstr "Applications"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "Theme"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:186
#: ../mate_menu/plugins/applications.py:187
msgid "Favorites"
msgstr "Favorites"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "Main button"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr "Plugins"

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "Background:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr "Headings:"

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "Borders:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "Theme:"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "Number of columns:"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "Icon size:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr "Hover delay (ms):"

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "Button icon:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "Search command:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:54
msgid "Places"
msgstr "Places"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr "Allow Scrollbar"

#: ../lib/mate-menu-config.py:100
msgid "Show GTK+ Bookmarks"
msgstr "Show GTK+ Bookmarks"

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "Height:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr "Toggle Default Places:"

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:149
msgid "Computer"
msgstr "Computer"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:160
msgid "Home Folder"
msgstr "Home Folder"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:173
msgid "Network"
msgstr "Network"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:196
msgid "Desktop"
msgstr "Desktop"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:207
msgid "Trash"
msgstr "Trash"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "Custom Places:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "System"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr "Toggle Default Items:"

#: ../lib/mate-menu-config.py:114 ../mate_menu/plugins/system_management.py:153
#: ../mate_menu/plugins/system_management.py:156
#: ../mate_menu/plugins/system_management.py:159
#: ../mate_menu/plugins/system_management.py:162
msgid "Package Manager"
msgstr "Package Manager"

#: ../lib/mate-menu-config.py:115 ../mate_menu/plugins/system_management.py:172
msgid "Control Center"
msgstr "Control Center"

#: ../lib/mate-menu-config.py:116 ../mate_menu/plugins/system_management.py:179
msgid "Terminal"
msgstr "Terminal"

#: ../lib/mate-menu-config.py:117 ../mate_menu/plugins/system_management.py:193
msgid "Lock Screen"
msgstr "Lock Screen"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "Log Out"

#: ../lib/mate-menu-config.py:119 ../mate_menu/plugins/system_management.py:211
msgid "Quit"
msgstr "Quit"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "Edit Place"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "New Place"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "Select a folder"

#: ../lib/mate-menu-config.py:151
msgid "Keyboard shortcut:"
msgstr "Keyboard shortcut:"

#: ../lib/mate-menu-config.py:157
msgid "Images"
msgstr "Images"

#: ../lib/mate-menu-config.py:265
msgid "Name"
msgstr "Name"

#: ../lib/mate-menu-config.py:266
msgid "Path"
msgstr "Path"

#: ../lib/mate-menu-config.py:282
msgid "Desktop theme"
msgstr "Desktop theme"

#: ../lib/mate-menu-config.py:423 ../lib/mate-menu-config.py:454
msgid "Name:"
msgstr "Name:"

#: ../lib/mate-menu-config.py:424 ../lib/mate-menu-config.py:455
msgid "Path:"
msgstr "Path:"

#. i18n
#: ../mate_menu/plugins/applications.py:184
msgid "Search:"
msgstr "Search:"

#: ../mate_menu/plugins/applications.py:188
msgid "All applications"
msgstr "All applications"

#: ../mate_menu/plugins/applications.py:552
#, python-format
msgid "Search Google for %s"
msgstr "Search Google for %s"

#: ../mate_menu/plugins/applications.py:559
#, python-format
msgid "Search Wikipedia for %s"
msgstr "Search Wikipedia for %s"

#: ../mate_menu/plugins/applications.py:576
#, python-format
msgid "Lookup %s in Dictionary"
msgstr "Lookup %s in Dictionary"

#: ../mate_menu/plugins/applications.py:583
#, python-format
msgid "Search Computer for %s"
msgstr "Search Computer for %s"

#. i18n
#: ../mate_menu/plugins/applications.py:703
#: ../mate_menu/plugins/applications.py:768
msgid "Add to desktop"
msgstr "Add to desktop"

#: ../mate_menu/plugins/applications.py:704
#: ../mate_menu/plugins/applications.py:769
msgid "Add to panel"
msgstr "Add to panel"

#: ../mate_menu/plugins/applications.py:706
#: ../mate_menu/plugins/applications.py:747
msgid "Insert space"
msgstr "Insert space"

#: ../mate_menu/plugins/applications.py:707
#: ../mate_menu/plugins/applications.py:748
msgid "Insert separator"
msgstr "Insert separator"

#: ../mate_menu/plugins/applications.py:709
#: ../mate_menu/plugins/applications.py:772
msgid "Launch"
msgstr "Launch"

#: ../mate_menu/plugins/applications.py:710
msgid "Remove from favorites"
msgstr "Remove from favorites"

#: ../mate_menu/plugins/applications.py:712
#: ../mate_menu/plugins/applications.py:775
msgid "Edit properties"
msgstr "Edit properties"

#. i18n
#: ../mate_menu/plugins/applications.py:746
msgid "Remove"
msgstr "Remove"

#: ../mate_menu/plugins/applications.py:771
msgid "Show in my favorites"
msgstr "Show in my favorites"

#: ../mate_menu/plugins/applications.py:773
msgid "Delete from menu"
msgstr "Delete from menu"

#: ../mate_menu/plugins/applications.py:817
msgid "Search Google"
msgstr "Search Google"

#: ../mate_menu/plugins/applications.py:824
msgid "Search Wikipedia"
msgstr "Search Wikipedia"

#: ../mate_menu/plugins/applications.py:834
msgid "Lookup Dictionary"
msgstr "Lookup Dictionary"

#: ../mate_menu/plugins/applications.py:841
msgid "Search Computer"
msgstr "Search Computer"

#: ../mate_menu/plugins/applications.py:1245
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"

#: ../mate_menu/plugins/applications.py:1452
msgid "All"
msgstr "All"

#: ../mate_menu/plugins/applications.py:1452
msgid "Show all applications"
msgstr "Show all applications"

#: ../mate_menu/plugins/system_management.py:167
msgid "Install, remove and upgrade software packages"
msgstr "Install, remove and upgrade software packages"

#: ../mate_menu/plugins/system_management.py:176
msgid "Configure your system"
msgstr "Configure your system"

#: ../mate_menu/plugins/system_management.py:190
msgid "Use the command line"
msgstr "Use the command line"

#: ../mate_menu/plugins/system_management.py:201
msgid "Requires password to unlock"
msgstr "Requires password to unlock"

#: ../mate_menu/plugins/system_management.py:204
msgid "Logout"
msgstr "Logout"

#: ../mate_menu/plugins/system_management.py:208
msgid "Log out or switch user"
msgstr "Log out or switch user"

#: ../mate_menu/plugins/system_management.py:215
msgid "Shutdown, restart, suspend or hibernate"
msgstr "Shutdown, restart, suspend or hibernate"

#: ../mate_menu/plugins/places.py:157
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr ""
"Browse all local and remote disks and folders accessible from this computer"

#: ../mate_menu/plugins/places.py:168
msgid "Open your personal folder"
msgstr "Open your personal folder"

#: ../mate_menu/plugins/places.py:181
msgid "Browse bookmarked and local network locations"
msgstr "Browse bookmarked and local network locations"

#: ../mate_menu/plugins/places.py:204
msgid "Browse items placed on the desktop"
msgstr "Browse items placed on the desktop"

#: ../mate_menu/plugins/places.py:217
msgid "Browse deleted files"
msgstr "Browse deleted files"

#: ../mate_menu/plugins/places.py:270
msgid "Empty trash"
msgstr "Empty trash"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "Recent documents"

#: ../mate_menu/keybinding.py:170
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr "Click to set a new accelerator key for opening and closing the menu.  "

#: ../mate_menu/keybinding.py:171
msgid "Press Escape or click again to cancel the operation.  "
msgstr "Press Escape or click again to cancel the operation.  "

#: ../mate_menu/keybinding.py:172
msgid "Press Backspace to clear the existing keybinding."
msgstr "Press Backspace to clear the existing keybinding."

#: ../mate_menu/keybinding.py:191
msgid "Pick an accelerator"
msgstr "Pick an accelerator"

#: ../mate_menu/keybinding.py:247
msgid "<not set>"
msgstr "<not set>"

#~ msgid "Opacity:"
#~ msgstr "Opacity:"

#~ msgid "Launch when I log in"
#~ msgstr "Launch when I log in"

#~ msgid "Please wait, this can take some time"
#~ msgstr "Please wait, this can take some time"

#~ msgid "Application removed successfully"
#~ msgstr "Application removed successfully"

#~ msgid ""
#~ "This menu item is not associated to any package. Do you want to remove it "
#~ "from the menu anyway?"
#~ msgstr ""
#~ "This menu item is not associated to any package. Do you want to remove it "
#~ "from the menu anyway?"

#~ msgid "The following packages will be removed:"
#~ msgstr "The following packages will be removed:"

#~ msgid "Packages to be removed"
#~ msgstr "Packages to be removed"

#~ msgid "Install package '%s'"
#~ msgstr "Install package '%s'"

#~ msgid "Uninstall"
#~ msgstr "Uninstall"

#~ msgid "Software Manager"
#~ msgstr "Software Manager"

#~ msgid "Browse and install available software"
#~ msgstr "Browse and install available software"

#~ msgid "Search for packages to install"
#~ msgstr "Search for packages to install"
